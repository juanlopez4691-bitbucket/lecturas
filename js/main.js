(function() {
    var elements = document.getElementsByClassName('_blank');

    Array.prototype.filter.call(elements, function(element){
        element.setAttribute('target', '_blank');
    });
})();
